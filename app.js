//CAROUSEL
const carouselSlide = document.querySelector('.carouselSlide');
const carouselImages = document.querySelectorAll('.carouselSlide img');

const prevBtn = document.querySelector('#previous');
const nextBtn = document.querySelector('#next');

let counter = 0;
const size = carouselImages[0].clientWidth;

carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';

nextBtn.addEventListener('click', () => {
    if (counter >= carouselImages.length - 1) return;
    carouselSlide.style.transition = 'transform 0.4s ease-in-out';
    counter++;
    carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';
});

prevBtn.addEventListener('click', () => {
    if (counter <= 0) return;
    carouselSlide.style.transition = 'transform 0.4s ease-in-out';
    counter--;
    carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';
});

//OFFERS
let offers = [{
        "id": 0,
        "city": "London",
        "price": 59,
        "description": "London is a capital city of United Kingdom. It is very beautifull place!",
        "image": "./images/london.png"
    },
    {
        "id": 1,
        "city": "Hamburg",
        "price": 79,
        "description": "Hamburg is one of the largest cities in northern Germany!",
        "image": "./images/hamburg.png"
    },
    {
        "id": 2,
        "city": "Alicante",
        "price": 89,
        "description": "Alicante is perfect place in Spain to rest yourself on the beach!",
        "image": "./images/alicante.png"
    },
    {
        "id": 3,
        "city": "Milano",
        "price": 119,
        "description": "If you like fashion, Milano is perfect place for you to visit!",
        "image": "./images/milano.png"
    },
]


function cityTemplate(city) {

    if (city.id == 0) {
        return `
      <div class="offer special">
      <div class="offerLabel">3h left at this rate <div id="triangle"></div></div>
      <img class="cityPhoto" src="${city.image}">
      <div class="offerText">
      <h2 class="cyty">${city.city}</h2>
      <p>from: &#163;${city.price}</p>
      <a cardId="${city.id}" onclick="modalElements(this)" class="bookBtn">Book now ></a>
      </div>
      </div>
    `;
    } else {

        return `
    <div class="offer">
    <img class="cityPhoto" src="${city.image}">
    <div class="offerText">
    <h2 class="cyty">${city.city}</h2>
    <p>from: &#163;${city.price}</p>
    <a cardId="${city.id}" onclick="modalElements(this)" class="bookBtn">Book now ></a>
    </div>
    </div>
        `;
    }
}

document.getElementById("offersWrapper").innerHTML = `

${offers.map(cityTemplate).join("")}

`;


//SERVICES TABS
const tabs = document.querySelectorAll('[data-tab-target]')
const tabContents = document.querySelectorAll('[data-tab-content]')

tabs.forEach(tab => {
    tab.addEventListener('click', () => {
        const target = document.querySelector(tab.dataset.tabTarget)
        tabContents.forEach(tabContent => {
            tabContent.classList.remove('active')
        })
        tabs.forEach(tab => {
            tab.classList.remove('active')
        })
        tab.classList.add('active')
        target.classList.add('active')
    })
});

//VALIDATE EMAIL
const email = document.querySelector('.email');
const update = document.querySelector('.status');

email.addEventListener('input', inputEmail);

function inputEmail(e) {
    const input = e.target.value;
    if (input && /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/.test(input)) {
        update.textContent = 'Valid Email!';
        update.classList.add('success');
        update.classList.remove('failure');
    } else {
        update.textContent = 'Keep Going...';
        update.classList.remove('success');
        update.classList.add('failure');
    }
    if (input === "") {
        update.textContent = 'Enter your email address';
        update.classList.remove('success');
        update.classList.remove('failure');

    }
};

//MODAL
let modalButtons = document.querySelectorAll(".bookBtn");
let modalOverlay = document.querySelector(".modalOverlay");
let modalClose = document.querySelector(".closeModal");
let modalBook = document.querySelector(".modalBook");

function openModal() {
    modalOverlay.classList.add("overlayActive")
}

function closeModal() {
    modalOverlay.classList.remove("overlayActive")
}

for (let i = 0; i < modalButtons.length; i++) {
    modalButtons[i].addEventListener("click", openModal);
    modalBook.addEventListener("click", closeModal);
    modalClose.addEventListener("click", closeModal);
}

let div = document.getElementById("modalData");

function modalElements(el) {
    let id = el.getAttribute("cardId")


    for (let i = 0; i < offers.length - (offers.length - 1); i++) {

        div.innerHTML = `
                <img src="./images/logo.png" alt="">
                <h2> Book your trip to <span> ${offers[id].city} </span> </h2>
                <p> ${offers[id].description} </p>
                <h3>For only <span>£${offers[id].price}</span></h3>
                <div class="input">
                <div class="inputData">
                    <input type="text" required>
                    <div class="underline"></div>
                    <label>Name and lastname</label>
                </div>
                <div class="inputData">
                    <input type="text" required>
                    <div class="underline"></div>
                    <label>Email address</label>
                </div>
                <div class="inputData">
                    <input type="number" required>
                    <div class="underline"></div>
                    <label>Phone number</label>
                </div>
                </div>
                
                `

    }
    console.log(id);
    console.log(el);
}


console.log(modalButtons);